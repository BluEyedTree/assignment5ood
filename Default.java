import java.util.*;
import javafx.util.Pair;

public class Default implements Mode {

	public ArrayList<Player> setPlayers(Observable o) {
		ArrayList<Player> plist = new ArrayList<Player>();
		plist.add(new HumanPlayer(o, "Player 1"));
		plist.add(new HumanPlayer(o, "Player 2"));
		return plist;
	}

	public ArrayList<Rule> setRules(Observable o) {
		ArrayList<Rule> rlist = new ArrayList<Rule>();
		rlist.add(new PlaceRule(o));
		rlist.add(new MoveRule(o));
                rlist.add(new PlaceWallRule(o));
                rlist.add(new MoveWallRule(o));
		return rlist;

	}

	public Map<Pair<Integer,Integer>,Piece> setBoard() {
                
                
		Map<Pair<Integer,Integer>,Piece> bm = new HashMap<Pair<Integer,Integer>,Piece>();
		for (int i=1; i<7; i++) {
			for (int i2=1; i2<7; i2++) {
                                Piece piece = new Piece(null, "none");
				bm.put(new Pair<Integer,Integer>(i,i2), piece);
			}
		}
		return bm;
	}

	public ArrayList<WinCondition> setWinConditions(Observable o) {
		ArrayList<WinCondition> wlist = new ArrayList<WinCondition>();
		wlist.add(new containsPath(o));
		wlist.add(new maxStones(o));
		return wlist;
	}

	public FooGUI setGUI(Observable o) {
		return new TextGUI(o);
	}
}
