
import java.util.Observable;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Tom
 */
public class PlaceWallRule extends Rule {
   
  public PlaceWallRule(Observable o) {
  	super(o);
  }

  public boolean check(Move m) {
    try {
      PlaceWall pw = (PlaceWall)m;
      if ((pw.getTarget().getKey() > 6) || (pw.getTarget().getValue() > 6)) {
      	return false;
      }
      if ((pw.getTarget().getKey() < 0) || (pw.getTarget().getValue() < 0)) {
      	return false;
      }
      //System.out.println(pw.getTarget());
      return boardstate.get(pw.getTarget()).owner == null;
    } catch (ClassCastException e) {
      return true;
    }
  }
}
