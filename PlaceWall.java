
import java.util.Map;
import javafx.util.Pair;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Tom
 */
public class PlaceWall extends Move {
      private Pair<Integer,Integer> target;
  
 

  public PlaceWall(Player p, Pair<Integer,Integer> c) {
    super(p);
    target = c;
  }

  public  Map<Pair<Integer,Integer>,Piece> alterBoard(Map<Pair<Integer,Integer>,Piece> boardstate) {
      //System.out.println(player);
      player.useStone(); //Removes a stone from the players pile
      Piece newWallPiece = new Piece(player,"wall");  
      boardstate.put(target, newWallPiece);
      return boardstate;
  }

  public Pair<Integer,Integer> getTarget() {
    return target;
  }
    
}
