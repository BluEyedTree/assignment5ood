import java.util.*;
import javafx.util.Pair;

public class PlaceRule extends Rule {

  public PlaceRule(Observable o) {
  	super(o);
  }

  public boolean check(Move m) {
    try {
      PlaceStone ps = (PlaceStone)m;
      if ((ps.getTarget().getKey() > 6) || (ps.getTarget().getValue() > 6)) {
      	return false;
      }
      if ((ps.getTarget().getKey() < 0) || (ps.getTarget().getValue() < 0)) {
      	return false;
      }
      //System.out.println(ps.getTarget());
      return boardstate.get(ps.getTarget()).owner == null;
    } catch (ClassCastException e) {
      return true;
    }
  }
}
