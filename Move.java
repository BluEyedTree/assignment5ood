import java.util.*;
import javafx.util.Pair;


public abstract class Move {
  protected Player player;

  public Move(Player p) {
    player = p;
  }

  public abstract Map<Pair<Integer,Integer>,Piece> alterBoard(Map<Pair<Integer,Integer>,Piece> boardstate);

  public Player getPlayer() {
    return player;
  }

}
