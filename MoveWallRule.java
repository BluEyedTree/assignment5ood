
import java.util.Observable;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Tom
 */
public class MoveWallRule extends Rule {
    
    public MoveWallRule(Observable o) {
    super(o);
  }

  public boolean check(Move m) {
    try {
      MoveWall mw = (MoveWall)m;

      if ((mw.getTarget().getKey( ) > 6) || (mw.getTarget().getValue() > 6)) {
        return false;
      }

      if ((mw.getOrigin().getKey( ) > 6) || (mw.getOrigin().getValue() > 6)) {
        return false;
      }

      if (boardstate.get(mw.getTarget()).owner==null) {
        return false;
      }
      if (boardstate.get(mw.getOrigin()).owner!=mw.getPlayer()){
        return false;
      }
      if (boardstate.get(mw.getTarget()).type.equals("wall")) {
        return false;
      }
      
      return FooGame.checkAdjacency(mw.getOrigin(), mw.getTarget());
    } catch (ClassCastException e) {
      return true;
    }
  }
    
}
