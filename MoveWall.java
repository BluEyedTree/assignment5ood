
import java.util.Map;
import javafx.util.Pair;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Tom
 */
public class MoveWall extends Move {
    
  private Pair<Integer,Integer> origin;
  private Pair<Integer,Integer> target;
 
  
  public MoveWall(Player p, Pair<Integer,Integer> c1, Pair<Integer,Integer> c2) {
    super(p);
   
    origin = c1;
    target = c2;
  }

  public  Map<Pair<Integer,Integer>,Piece> alterBoard(Map<Pair<Integer,Integer>,Piece> boardstate) {
      //Does inputPlayer work??
      Piece pieceInOldPlace = new Piece(null,"none");
      boardstate.put(target, boardstate.get(origin));
      boardstate.put(origin, pieceInOldPlace);
      return boardstate;
  }

  public Pair<Integer,Integer> getOrigin() {
    return origin;
  }

  public Pair<Integer,Integer> getTarget() {
    return target;
  }
    
}
